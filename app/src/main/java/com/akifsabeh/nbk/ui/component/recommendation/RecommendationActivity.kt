package com.akifsabeh.nbk.ui.component.recommendation

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import android.widget.SeekBar
import com.akifsabeh.nbk.R
import com.akifsabeh.nbk.data.HistoricalData
import com.akifsabeh.nbk.utils.ValueFormater
import com.akifsabeh.nbk.views.MyMarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import kotlinx.android.synthetic.main.activity_recommendation.*
import kotlinx.android.synthetic.main.historical_chart.*
import kotlinx.android.synthetic.main.recommendation_header.*
import kotlinx.android.synthetic.main.recommendation_tolerance.*
import java.util.*


/**
 * Created by Akif sabeh on 10/17/2018.
 */


class RecommendationActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener, OnChartValueSelectedListener {


    private var map: HashMap<Int, HashMap<Int, ArrayList<Entry>>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recommendation)

        //dummy home button and icon
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)

        map = HistoricalData.generateData()


        seekbar_tolerance.setOnSeekBarChangeListener(this)

        //initChart()
        initChart()

        //default tolerance 9
        onToleranceProgressChanged(9)

        //align recommended text to 9th position
        alignRecommendedText()


        //show-hide header title text
        ll_header.setOnClickListener { updateHeaderTitle() }
        btn_decrement_tolerance.setOnClickListener { decrementTolerance() }
        btn_increment_tolerance.setOnClickListener { incrementTolerance() }
        btn_show_history.setOnClickListener { showHideHistory() }

    }

    private fun alignRecommendedText() {
        seekbar_tolerance.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                seekbar_tolerance.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val width =
                        resources.getDimensionPixelOffset(R.dimen.seekbar_thumb_width) + resources.getDimensionPixelOffset(
                                R.dimen.size_24_dp
                        )

                val params = txt_recommended.layoutParams as FrameLayout.LayoutParams
                params.setMargins(0, 0, width, 0) //substitute parameters for left, top, right, bottom
                txt_recommended.layoutParams = params

            }
        })


    }

    private fun showHideHistory() {
        if (ll_history.visibility == View.VISIBLE) {
            ll_history.visibility = View.GONE
            btn_show_history.text = getString(R.string.action_view_historical_performance)
        } else {
            ll_history.visibility = View.VISIBLE
            btn_show_history.text = getString(R.string.action_hide_historical_performance)
            val handler = Handler()
            handler.postDelayed({
                scrollView.smoothScrollTo(0, ll_history.bottom)
            }, 200)


        }
    }


    private fun incrementTolerance() {
        if (seekbar_tolerance.progress < 9)
            seekbar_tolerance.progress = seekbar_tolerance.progress + 1

    }

    private fun decrementTolerance() {
        if (seekbar_tolerance.progress > -1)
            seekbar_tolerance.progress = seekbar_tolerance.progress - 1
    }

    private fun updateHeaderTitle() {
        if (txt_header_body.visibility == View.VISIBLE) {
            txt_header_body.visibility = View.GONE
            txt_header_title.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_keyboard_arrow_down_black_24dp,
                    0
            )
        } else {
            txt_header_body.visibility = View.VISIBLE
            txt_header_title.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_keyboard_arrow_up_black_24dp,
                    0
            )
        }

    }


    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        onToleranceProgressChanged(progress + 1)

    }

    private fun onToleranceProgressChanged(p: Int) {
        txt_tolerance.text = getString(R.string.label_risk_tolerance, String.format("%.1f", p.toDouble()))
        when (p) {
            1, 3, 7 -> updateStockProgress(10, 20, 20, 40, 5, 5)
            2, 5, 8, 10 -> updateStockProgress(0, 40, 20, 20, 10, 10)
            4, 6, 9 -> updateStockProgress(70, 10, 5, 5, 0, 10)
        }
        setHistoryData(p)
    }


    private fun updateStockProgress(
            usStock: Int,
            developedMarketStock: Int,
            emergingMarketStock: Int,
            usBonds: Int,
            globalBonds: Int,
            cash: Int
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pr_us_stock.setProgress(usStock, true)
            pr_developed_market_stock.setProgress(developedMarketStock, true)
            pr_emerging_market_stock.setProgress(emergingMarketStock, true)
            pr_us_bonds.setProgress(usBonds, true)
            pr_global_bonds.setProgress(globalBonds, true)
            pr_cash_bonds.setProgress(cash, true)

        } else {
            pr_us_stock.progress = usStock
            pr_developed_market_stock.progress = developedMarketStock
            pr_emerging_market_stock.progress = emergingMarketStock
            pr_us_bonds.progress = usBonds
            pr_global_bonds.progress = globalBonds
            pr_cash_bonds.progress = cash
        }

        txt_us_stock_value.text =
                getString(R.string.label_progress_percentage, String.format("%.1f", usStock.toDouble()))
        txt_developed_market_value.text =
                getString(R.string.label_progress_percentage, String.format("%.1f", developedMarketStock.toDouble()))
        txt_emerging_market_stock.text =
                getString(R.string.label_progress_percentage, String.format("%.1f", emergingMarketStock.toDouble()))
        txt_us_bonds_value.text =
                getString(R.string.label_progress_percentage, String.format("%.1f", usBonds.toDouble()))
        txt_global_bonds_value.text =
                getString(R.string.label_progress_percentage, String.format("%.1f", globalBonds.toDouble()))
        txt_cash_value.text = getString(R.string.label_progress_percentage, String.format("%.1f", cash.toDouble()))
    }


    private fun initChart() {

        chart.setOnChartValueSelectedListener(this)


        // no description text
        chart.description.isEnabled = false

        // enable touch gestures
        chart.setTouchEnabled(true)

        chart.dragDecelerationFrictionCoef = 0.9f

        // enable scaling and dragging
        chart.isDragEnabled = true
        chart.setScaleEnabled(false)
        chart.setDrawGridBackground(false)
        chart.isHighlightPerDragEnabled = true
        chart.setDrawBorders(true)
        chart.setBorderColor(ContextCompat.getColor(this, R.color.backgroundGray))

        val mv = MyMarkerView(this, R.layout.custom_marker_view)

        // Set the marker to the chart
        mv.chartView = chart
        chart.marker = mv

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true)

        chart.setBackgroundColor(Color.WHITE)


        chart.axisLeft.setDrawLabels(false)




        chart.animateX(1500)

        val l = chart.legend
        l.isEnabled = false


        val xAxis = chart.xAxis
        xAxis.textSize = 11f
        xAxis.textColor = ContextCompat.getColor(this, R.color.text_color_light1)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.valueFormatter = ValueFormater()
        xAxis.gridColor = ContextCompat.getColor(this, R.color.backgroundGray)
        xAxis.gridLineWidth = 2f
        xAxis.setDrawAxisLine(false)

        val rightAxis = chart.axisRight
        rightAxis.textColor = ContextCompat.getColor(this, R.color.text_color_light1)
        rightAxis.textSize = 11f
        rightAxis.gridColor = ContextCompat.getColor(this, R.color.backgroundGray)
        rightAxis.gridLineWidth = 2f
        rightAxis.setDrawAxisLine(false)


    }

    private fun setHistoryData(value: Int) {

        val values1 = map?.get(0)!![value]
        val values2 = map?.get(1)!![value]


        val set1: LineDataSet
        val set2: LineDataSet

        if (chart.data != null && chart.data.dataSetCount > 0) {
            set1 = chart.data.getDataSetByIndex(0) as LineDataSet
            set2 = chart.data.getDataSetByIndex(1) as LineDataSet
            set1.values = values1
            set2.values = values2
            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
            chart.invalidate()
        } else {
            set1 = LineDataSet(values1, getString(R.string.label_smart_wealth))

            set1.color = ContextCompat.getColor(this, R.color.green_dot)
            set1.setCircleColor(Color.WHITE)
            set1.lineWidth = 1f
            set1.fillAlpha = 65
            set1.fillColor = ContextCompat.getColor(this, R.color.green_dot)
            set1.highLightColor = ContextCompat.getColor(this, R.color.green_dot)
            set1.setDrawCircleHole(false)
            set1.setDrawCircles(false)
            set1.setDrawValues(false)
            set1.isHighlightEnabled = true
            set1.setDrawFilled(true)
            set1.setDrawHorizontalHighlightIndicator(false)
            set1.highLightColor = Color.WHITE

            set2 = LineDataSet(values2, getString(R.string.label_benchMark))
            set2.color = ContextCompat.getColor(this, R.color.blue_dot)
            set2.lineWidth = 1f
            set2.fillAlpha = 65
            set2.fillColor = ContextCompat.getColor(this, R.color.blue_dot)
            set2.setDrawCircleHole(false)
            set2.highLightColor = Color.WHITE
            set2.setDrawCircles(false)
            set2.setDrawValues(false)
            set2.setDrawFilled(true)
            set2.isHighlightEnabled = true
            set2.setDrawHorizontalHighlightIndicator(false)
            val data = LineData(set1, set2)
            data.setValueTextColor(Color.WHITE)
            data.setValueTextSize(9f)
            chart.data = data
        }

    }

    override fun onNothingSelected() {

    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        var smartWealth = 0F
        var benchMark = 0F
        val highlight = arrayOfNulls<Highlight>(chart.data.dataSets.size)
        for (j in 0 until chart.data.dataSets.size) {

            val iDataSet = chart.data.dataSets[j]

            for (i in 0 until (iDataSet as LineDataSet).values.size) {
                if (iDataSet.values[i].x == e?.x) {
                    if (iDataSet.label == getString(R.string.label_smart_wealth))
                        smartWealth = iDataSet.values[i].y
                    else if (iDataSet.label == getString(R.string.label_benchMark))
                        benchMark = iDataSet.values[i].y
                    highlight[j] = Highlight(e.x, e.y, j)
                }
            }

        }
        txt_smart_wealth.text = getString(R.string.label_gain_amount, smartWealth.toString())
        txt_benchmark.text = getString(R.string.label_gain_amount, benchMark.toString())
        chart.highlightValues(highlight)
    }

}
