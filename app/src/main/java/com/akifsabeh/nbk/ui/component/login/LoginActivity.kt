package com.akifsabeh.nbk.ui.component.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.akifsabeh.nbk.R
import com.akifsabeh.nbk.utils.NavigationUtils
import com.akifsabeh.nbk.utils.ToastUtils
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by Akif sabeh on 10/17/2018.
 */

class LoginActivity : AppCompatActivity() {
    private val RC_SIGN_IN: Int = 99
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        btn_login.setOnClickListener { login() }

        checkIfLoggedIn()
    }

    private fun checkIfLoggedIn() {
        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (account != null)
            loginSuccess()
    }

    private fun login() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful
                val account = task.getResult(ApiException::class.java)
                loginSuccess()
            } catch (e: ApiException) {
                ToastUtils.showoast(this, getString(R.string.error_sign_in), Toast.LENGTH_SHORT)
            }
        }
    }

    private fun loginSuccess() {
        NavigationUtils.startRecommendationActivity(this)
        finish()
    }
}
