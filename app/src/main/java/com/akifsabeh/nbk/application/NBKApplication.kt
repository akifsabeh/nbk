package com.akifsabeh.nbk.application

import android.support.multidex.MultiDexApplication

/**
 * Created by Akif sabeh on 11/17/2018.
 */
class NBKApplication : MultiDexApplication() {
}