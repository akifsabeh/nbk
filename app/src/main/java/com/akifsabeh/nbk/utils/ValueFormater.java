package com.akifsabeh.nbk.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by Akif sabeh on 11/18/2018.
 */
public class ValueFormater implements IAxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return new DecimalFormat("#########").format(value);
    }
}
