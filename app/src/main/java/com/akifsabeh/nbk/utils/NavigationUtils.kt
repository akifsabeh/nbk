package com.akifsabeh.nbk.utils

import android.content.Context
import android.content.Intent
import com.akifsabeh.nbk.ui.component.recommendation.RecommendationActivity

/**
 * Created by Akif sabeh on 11/16/2018.
 */
class NavigationUtils {

    companion object {
        fun startRecommendationActivity(context: Context) {
            context.startActivity(Intent(context, RecommendationActivity::class.java))
        }
    }
}