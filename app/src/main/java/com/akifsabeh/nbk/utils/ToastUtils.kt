package com.akifsabeh.nbk.utils

import android.content.Context
import android.widget.Toast

/**
 * Created by Akif sabeh on 11/16/2018.
 */
class ToastUtils {

    companion object {
        fun showoast(context: Context, msg: String, duration: Int) {
            Toast.makeText(context, msg, duration).show()
        }
    }
}