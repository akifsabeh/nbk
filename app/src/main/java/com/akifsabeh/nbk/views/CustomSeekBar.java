package com.akifsabeh.nbk.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import com.akifsabeh.nbk.R;

/**
 * Created by Akif sabeh on 11/18/2018.
 */
public class CustomSeekBar extends AppCompatSeekBar {
    private Paint p;

    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int offset = this.getThumb().getMinimumWidth();
        float interval = (float) ((getWidth() - offset * 1.0) / 9);
        if (p == null) {
            p = new Paint();
            p.setColor(ContextCompat.getColor(getContext(), R.color.divider));
            p.setStrokeWidth(getResources().getDimension(R.dimen.divider_size));
        }

        float x = 8 * interval + offset / 2;
        canvas.drawLine(x, 0, x, getHeight(), p);

    }
}
