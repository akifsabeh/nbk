package com.akifsabeh.nbk.views;

/**
 * Created by Akif sabeh on 11/18/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;


@SuppressLint("ViewConstructor")
public class MyMarkerView extends MarkerView {


    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight() / 2);
    }
}